package com.github.taucher2003.guilded4j.internal.net.http;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.github.taucher2003.guilded4j.internal.net.Route;
import com.github.taucher2003.guilded4j.internal.net.Route.Method;

public class HTTPRouteTest {
	@Test
	@DisplayName("Check path method and formatting")
	public void checkPathFormatting() {
		Route route = new HTTPRoute(Method.GET, "out/${target}");
		
		Map<String, String> options = new HashMap<>();
		options.put("target", "default");
		
		assertEquals(route.getMethod(), Method.GET);
		assertEquals(route.getFormatted(options), "out/default");
		assertEquals(route.getFormatted("default"), "out/default");
		assertEquals(route.getFormatted(), "out/${target}");
	}
}
