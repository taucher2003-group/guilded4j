package com.github.taucher2003.guilded4j.internal.net.http;

import java.util.Map;

import com.github.taucher2003.guilded4j.internal.net.Route;

public class HTTPRoute implements Route {

	private final Method method;
	private final String path;

	public HTTPRoute(Method method, String path) {
		this.method = method;
		this.path = path;
	}
	
	@Override
	public String getRaw() {
		return path;
	}
	
	@Override
	public Method getMethod() {
		return method;
	}

	@Override
	public String getFormatted(String... options) {
		String formatted = path;
		
		for (int i=0; i<options.length; i++) {
			formatted = formatted.replaceFirst("\\$\\{\\w+}", options[i]);
		}
		
		return formatted;
	}

	@Override
	public String getFormatted(Map<String, String> options) {
		String formatted = path;
		
		for (String key: options.keySet()) {
			formatted = formatted.replace("${"+key+"}", options.get(key));
		};
		
		return formatted;
	}
	
}
