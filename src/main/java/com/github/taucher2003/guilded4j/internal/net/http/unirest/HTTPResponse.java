package com.github.taucher2003.guilded4j.internal.net.http.unirest;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.github.taucher2003.guilded4j.internal.net.Response;
import com.mashape.unirest.http.HttpResponse;

public class HTTPResponse implements Response {
	private final HttpResponse<String> response;

	public HTTPResponse(HttpResponse<String> response) {
		this.response = response;
	}

	@Override
	public int getStatusCode() {
		return response.getStatus();
	}

	@Override
	public Optional<String> getBody() {
		return Optional.of(response.getBody());
	}

	@Override
	public Map<String, String> getHeaders() {
		Map<String, String> headers = new HashMap<>();
		response.getHeaders().forEach((name, l)->headers.put(name, l.get(0)));
		return headers;
	}
}
