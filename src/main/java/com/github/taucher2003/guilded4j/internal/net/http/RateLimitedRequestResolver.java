package com.github.taucher2003.guilded4j.internal.net.http;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

import com.github.taucher2003.guilded4j.internal.net.Request;
import com.github.taucher2003.guilded4j.internal.net.RequestResolver;
import com.github.taucher2003.guilded4j.internal.net.Response;

import reactor.core.publisher.Mono;

public class RateLimitedRequestResolver implements RequestResolver {

	private final RequestResolver underlyingResolver;
	private final AtomicInteger globalRateLimit = new AtomicInteger(0);

	public RateLimitedRequestResolver(RequestResolver underlyingResolver) {
		this.underlyingResolver = underlyingResolver;
	}

	@Override
	public Mono<Response> submit(Request request) {
		return Mono.empty().repeatWhenEmpty(f->{
			final AtomicInteger localRateLimit = new AtomicInteger(0);
			
			return
				Mono.delay(Duration.ofMillis(localRateLimit.get()))
				//TODO: Log rate limits greater than 0
				.then(Mono.delay(Duration.ofMillis(globalRateLimit.get())))
				.then(underlyingResolver.submit(request));
				//TODO: Handle rate limits
		}).cast(Response.class);
	}
}
