package com.github.taucher2003.guilded4j.internal.net.http.unirest;

import com.github.taucher2003.guilded4j.internal.net.Request;
import com.github.taucher2003.guilded4j.internal.net.RequestResolver;
import com.github.taucher2003.guilded4j.internal.net.Response;
import com.github.taucher2003.guilded4j.internal.net.Route.Method;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

import reactor.core.publisher.Mono;

public class HTTPRequestResolver implements RequestResolver {
	private String origin;

	public HTTPRequestResolver(String origin) {
		this.origin = origin;
	}
	
	@Override
	public Mono<Response> submit(Request request) {
		final String url = origin+'/'+request.getRoute().getFormatted(request.getParameters());
		
		return Mono.create(sink->{
			HttpRequest unirequest = null;
			Method method = request.getRoute().getMethod();
			
			switch (method) {
				case CONNECT:
					throw new UnsupportedOperationException("Method "+method.toString()+" not supported");
				case DELETE:
					unirequest = Unirest.delete(url);
					break;
				case GET:
					unirequest = Unirest.get(url);
					break;
				case HEAD:
					unirequest = Unirest.head(url);
					break;
				case OPTIONS:
					unirequest = Unirest.options(url);
					break;
				case POST:
					unirequest = Unirest.post(url);
					break;
				case PUT:
					unirequest = Unirest.put(url);
					break;
				case TRACE:
					throw new UnsupportedOperationException("Method "+method.toString()+" not supported");
			}
			
			if (unirequest instanceof HttpRequestWithBody) {
				((HttpRequestWithBody) unirequest).body(request.getBody());
			}
			
			unirequest.asStringAsync(new Callback<String>() {
				@Override
				public void completed(HttpResponse<String> response) {
					sink.success(new HTTPResponse(response));
				}
				
				@Override
				public void failed(UnirestException e) {
					sink.error(e);
				}
				
				@Override
				public void cancelled() {
					sink.success();
				}
			});
		});
	}
}
