/*
 * Copyright 2021 Niklas van Schrick, Jason Gronn, Troy Pierce and the Guilded4J contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.github.taucher2003.guilded4j.internal.util;

import com.github.taucher2003.t2003_logger.T2003Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class GuildedLogger {

    private static final Map<String, Logger> LOGGERS = new ConcurrentHashMap<>();
    private static Boolean SLF4J_USED = null;

    public static Logger getLogger(Class<?> clazz) {
        return getLogger(clazz.getCanonicalName());
    }

    public static Logger getLogger(String name) {
        if(SLF4J_USED == null) {
            try {
                Class.forName("org.slf4j.impl.StaticLoggerBinder");
                SLF4J_USED = true;
            }catch (ClassNotFoundException ignored) {
                System.err.println("SLF4J: Failed to load class \"org.slf4j.impl.StaticLoggerBinder\"");
                System.err.println("SLF4J: Falling back to default Logger");
                SLF4J_USED = false;
            }
        }
        if(SLF4J_USED)
            return LOGGERS.computeIfAbsent(name, LoggerFactory::getLogger);
        return LOGGERS.computeIfAbsent(name, T2003Logger::new);
    }
}
