package com.github.taucher2003.guilded4j.internal.net;

import java.util.Map;
import java.util.Optional;

/**
 * Represents a response to a sent request.
 */
public interface Response {
	
	/**
	 * Get the status code of this response.
	 * @return The status code of this response
	 */
	public int getStatusCode();
	
	/**
	 * Get the body of this response.
	 * @return Optionally, the body of this response
	 */
	public Optional<String> getBody();
	
	/**
	 * Get the headers sent with this response.
	 * @return A map of headers sent with this response
	 */
	public Map<String, String> getHeaders();
}
