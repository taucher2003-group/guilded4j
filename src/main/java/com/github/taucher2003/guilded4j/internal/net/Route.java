package com.github.taucher2003.guilded4j.internal.net;

import java.util.Map;

public interface Route {
	
	/**
	 * Get the path string without any of it's fields filled in.
	 * @return The raw path string
	 */
	String getRaw();
	
	/**
	 * Get the method of this path.
	 * @return the method of this path
	 */
	Method getMethod();
	
	/**
	 * Get the path string, with the specified fields.
	 * @param options An array of alternating field names and field values to be filled in.
	 * @return The formatted path string
	 */
	String getFormatted(String... options);
	
	/**
	 * Get the path string, with the specified fields.
	 * @param options A map containing field names and field values to be filled in.
	 * @return The formatted path string
	 */
	String getFormatted(Map<String, String> options);
	
	enum Method {
		GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE
	}
}
