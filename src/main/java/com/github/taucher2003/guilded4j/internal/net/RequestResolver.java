package com.github.taucher2003.guilded4j.internal.net;

import reactor.core.publisher.Mono;

public interface RequestResolver {
	
	/**
	 * Attempts to submit a request.
	 * Requests are not guaranteed to be sent in the order submitted.
	 * @param request The request to be submitted.
	 * @return The response to the request
	 */
	Mono<Response> submit(Request request);
	
}
