package com.github.taucher2003.guilded4j.internal.net;

import java.util.Map;

/**
 * A request to be sent to a path on a server.
 */
public interface Request {
	/**
	 * Get the route that this request will hit.
	 * @return The route that this request targets
	 */
	public Route getRoute();
	
	/**
	 * Get the parameters of this request.
	 * @return A map containing the parameters of this request
	 */
	public Map<String, String> getParameters();
	
	/**
	 * Get the headers that will be sent with this request.
	 * @return A map of the headers that will be sent with this request
	 */
	public Map<String, String> getHeaders();

	/**
	 * Get the body that will be sent with this request.
	 * @return The body that will be sent with this request
	 */
	public String getBody();
}
