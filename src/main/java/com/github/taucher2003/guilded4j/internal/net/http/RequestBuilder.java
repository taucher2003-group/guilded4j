package com.github.taucher2003.guilded4j.internal.net.http;

import java.util.Collections;
import java.util.Map;

import com.github.taucher2003.guilded4j.internal.net.Request;
import com.github.taucher2003.guilded4j.internal.net.Route;

public class RequestBuilder {
	private Route route;
	private Map<String, String> headers = Collections.emptyMap();
	private Map<String, String> parameters = Collections.emptyMap();
	private String body = "";

	public void setRoute(Route route) {
		this.route = route;
	}
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
	
	public Request build() {
		if (route==null) {
			throw new RuntimeException("Path not specified");
		}
		
		return new Request() {
			@Override
			public Route getRoute() {
				return route;
			}

			@Override
			public Map<String, String> getParameters() {
				return parameters;
			}

			@Override
			public Map<String, String> getHeaders() {
				return headers;
			}

			@Override
			public String getBody() {
				return body;
			}
		};
	}
}
